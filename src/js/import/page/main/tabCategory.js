import $ from "jquery";
import Swiper from "swiper";

// $(document).ready(function () {
//     console.log("Ghbdtn мир");
//   //initialize swiper when document ready
//   var mySwiper = new Swiper ('.bm-tab-cart__slider', {
//       // Optional parameters
//       direction: 'horizontal',
//       speed: 800,
//       spaceBetween: 20,
//       slidesPerView: 4,
  
//       // Navigation arrows
//       navigation: {
//         nextEl: '.bm-tab-cart__button-next',
//         prevEl: '.bm-tab-cart__button-prev',
//       },


  
//     });
// });
var slideWidth = $(window).width();

if (slideWidth > 319 && slideWidth < 767) {
  $('.bm-tab-cart__select').on('click', function(){
    $('.bm-tab-cart__header-tab').slideToggle();
    $('.bm-tab-cart__select').toggleClass('active-tab ');
  })
  
  $('.bm-tab-cart__header-tab_title').on('click', function() {
    $('.bm-tab-cart__header-tab').slideToggle();
    $('.bm-tab-cart__select').toggleClass('active-tab ');
   })
}


$('.bm-js-tab-trigger').on('click', function(){
    let tabName = $(this).data('tab');
    let tab = $('.js-tab-content[data-tab="'+tabName+'"]');
    let topTab = $('.bm-js-tab-trigger[data-tab="'+tabName+'"]');

    $('.bm-tab-cart__active.bm-js-tab-trigger').removeClass('bm-tab-cart__active');
    $(this).addClass('bm-tab-cart__active');

    $('.bm-tab-cart__active.js-tab-content').removeClass('bm-tab-cart__active');
    tab.addClass('bm-tab-cart__active');
     if ($('.bm-tab-cart__select').text() !== '') {
       console.log('Проверка');
      $('.bm-tab-cart__select').text('');
      $('.bm-tab-cart__select').append(topTab.text());
     } else {
      $('.bm-tab-cart__select').append(topTab.text());
     }
     
     console.log($('.bm-tab-cart__select').text() !== '');
})

const mySwipers = {};

function initSwiper(id, idname) {
    let allslider = $(id).find('.swiper-container');


    $.each(allslider, function(key, val) {
        var selecto = id +' #'+ $(val).attr('id');
        // mySwipers[idname] = new Swiper ('.bm-tab-cart__slider', {
        //     // Optional parameters
        //     direction: 'horizontal',
        //     speed: 800,
        //     spaceBetween: 20,
        //     slidesPerView: 4,
        
        //     // Navigation arrows
        //     navigation: {
        //       nextEl: '.bm-tab-cart__button-next',
        //       prevEl: '.bm-tab-cart__button-prev',
        //     },
      
      
        
        //   });

          
          if (slideWidth > 1023 && slideWidth < 1440) {
            mySwipers[idname] = new Swiper ('.bm-tab-cart__slider', {
              // Optional parameters
              direction: 'horizontal',
              speed: 800,
              spaceBetween: 20,
              slidesPerView: 3,
          
              // Navigation arrows
              navigation: {
                nextEl: '.bm-tab-cart__button-next',
                prevEl: '.bm-tab-cart__button-prev',
              },
        
        
          
            });
          } else if (slideWidth > 767 && slideWidth < 1023) {
            mySwipers[idname] = new Swiper ('.bm-tab-cart__slider', {
              // Optional parameters
              direction: 'horizontal',
              speed: 800,
              spaceBetween: 20,
              slidesPerView: 2,
          
              // Navigation arrows
              navigation: {
                nextEl: '.bm-tab-cart__button-next',
                prevEl: '.bm-tab-cart__button-prev',
              },
        
        
          
            });
          } else if (slideWidth > 319 && slideWidth < 767) {
            mySwipers[idname] = new Swiper ('.bm-tab-cart__slider', {
              // Optional parameters
              direction: 'horizontal',
              speed: 800,
              spaceBetween: 20,
              slidesPerView: 1,
          
              // Navigation arrows
              navigation: {
                nextEl: '.bm-tab-cart__button-next',
                prevEl: '.bm-tab-cart__button-prev',
              },
        
        
          
            });
          } else {
              mySwipers[idname] = new Swiper ('.bm-tab-cart__slider', {
                // Optional parameters
                direction: 'horizontal',
                speed: 800,
                spaceBetween: 20,
                slidesPerView: 4,
            
                // Navigation arrows
                navigation: {
                  nextEl: '.bm-tab-cart__button-next',
                  prevEl: '.bm-tab-cart__button-prev',
                },
          
          
            
              });
            }
      });
}

// function tabs() {
//     let tabName = $(this).data('tab');
//     let tab = $('.js-tab-content[data-tab="'+tabName+'"]');

//     $('.bm-tab-cart__active.bm-js-tab-trigger').removeClass('bm-tab-cart__active');
//     $(this).addClass('bm-tab-cart__active');

//     $('.bm-tab-cart__active.js-tab-content').removeClass('bm-tab-cart__active');
//     tab.addClass('bm-tab-cart__active');
// }

$('#myTab a').on('click', function (e) {
    e.preventDefault()
        
    var self = this;
    var id = $(e.target).attr('href');

    var re = /#/gi;
    var idname = id.replace(re, '');
        
    //   $(self).tab('show');
   
    
    // tabs();
    initSwiper(id,idname);
     
  });

  initSwiper('#slider1','slider1');