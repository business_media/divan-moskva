import $ from "jquery";
import Swiper from "swiper";

// $(document).ready(function () {
//   //initialize swiper when document ready
//   // var mySwiper = new Swiper ('.bm-featured__wrapper', {
//   //     // Optional parameters
//   //     direction: 'horizontal',
//   //     loop: true,
//   //     speed: 800,
//   //     spaceBetween: 30,
//   //     slidesPerView: 4,
  
//   //     // Navigation arrows
//   //     navigation: {
//   //       nextEl: '.bm-featured__button-next',
//   //       prevEl: '.bm-featured__button-prev',
//   //     },
//   //   });
  
// });

var slideWidth = $(window).width();

if (slideWidth > 1023 && slideWidth < 1439) {
  var mySwiper = new Swiper ('.bm-featured__wrapper', {
    // Optional parameters
    direction: 'horizontal',
    loop: true,
    speed: 800,
    spaceBetween: 30,
    slidesPerView: 3,

    // Navigation arrows
    navigation: {
      nextEl: '.bm-featured__button-next',
      prevEl: '.bm-featured__button-prev',
    },
  });
} else if (slideWidth > 767 && slideWidth < 1023)  {
  var mySwiper = new Swiper ('.bm-featured__wrapper', {
    // Optional parameters
    direction: 'horizontal',
    loop: true,
    speed: 800,
    spaceBetween: 30,
    slidesPerView: 2,

    // Navigation arrows
    navigation: {
      nextEl: '.bm-featured__button-next',
      prevEl: '.bm-featured__button-prev',
    },
  });
} else if (slideWidth > 319 && slideWidth < 767) {
  var mySwiper = new Swiper ('.bm-featured__wrapper', {
    // Optional parameters
    direction: 'horizontal',
    loop: true,
    speed: 800,
    spaceBetween: 30,
    slidesPerView: 1,

    autoplay: {
      delay: 5000,
    },

    // Navigation arrows
    navigation: {
      nextEl: '.bm-featured__button-next',
      prevEl: '.bm-featured__button-prev',
    },
  });
} else {
  console.log("Тестировка")
  var mySwiper = new Swiper ('.bm-featured__wrapper', {
    // Optional parameters
    direction: 'horizontal',
    loop: true,
    speed: 800,
    spaceBetween: 30,
    slidesPerView: 4,

    // Navigation arrows
    navigation: {
      nextEl: '.bm-featured__button-next',
      prevEl: '.bm-featured__button-prev',
    },
  });
}