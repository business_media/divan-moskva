import $ from "jquery";
import Swiper from "swiper";

// $(document).ready(function () {
//     console.log("Ghbdtn мир");
//   //initialize swiper when document ready
//   var mySwiper = new Swiper ('.bm-company-label__wrapper', {
//       // Optional parameters
//       direction: 'horizontal',
      
//       speed: 800,
//       spaceBetween: 40,
//       slidesPerView: 6,
  
//       // Navigation arrows
//       navigation: {
//         nextEl: '.bm-company-label__button-next',
//         prevEl: '.bm-company-label__button-prev',
//       },


  
//     });
// });

var slideWidth = $(window).width();

if (slideWidth > 1023 && slideWidth < 1439) {
    //initialize swiper when document ready
    var mySwiper = new Swiper ('.bm-company-label__wrapper', {
      // Optional parameters
      direction: 'horizontal',
      
      speed: 800,
      spaceBetween: 35,
      slidesPerView: 5,
  
      // Navigation arrows
      navigation: {
        nextEl: '.bm-company-label__button-next',
        prevEl: '.bm-company-label__button-prev',
      },


  
    });
} else if (slideWidth > 767 && slideWidth < 1023) {
  var mySwiper = new Swiper ('.bm-company-label__wrapper', {
    // Optional parameters
    direction: 'horizontal',
    
    speed: 800,
    spaceBetween: 25,
    slidesPerView: 4,

    autoplay: {
      delay: 5000,
    },

    // Navigation arrows
  });
} else if (slideWidth > 319 && slideWidth < 767) {
  var mySwiper = new Swiper ('.bm-company-label__wrapper', {
    // Optional parameters
    direction: 'horizontal',
    
    speed: 800,
    spaceBetween: 5,
    slidesPerView: 2,

    autoplay: {
      delay: 5000,
    },

    // Navigation arrows
  });
} else {
  var mySwiper = new Swiper ('.bm-company-label__wrapper', {
    // Optional parameters
    direction: 'horizontal',
    
    speed: 800,
    spaceBetween: 40,
    slidesPerView: 6,

    // Navigation arrows
    navigation: {
      nextEl: '.bm-company-label__button-next',
      prevEl: '.bm-company-label__button-prev',
    },



  });
}