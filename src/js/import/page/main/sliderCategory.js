import $ from "jquery";
import Swiper from "swiper";

// $(document).ready(function () {
//     console.log("Ghbdtn мир");
//   //initialize swiper when document ready
//   var mySwiper = new Swiper ('.bm-slider-category', {
//       // Optional parameters
//       direction: 'horizontal',
//       loop: true,
//       speed: 800,
//       spaceBetween: 20,
//       slidesPerView: 5,
  
//       // Navigation arrows
//       navigation: {
//         nextEl: '.bm-slider-category__button-next',
//         prevEl: '.bm-slider-category__button-prev',
//       },


  
//     });
// });

var slideWidth = $(window).width();

if (slideWidth > 1023 && slideWidth < 1439) {
  console.log("Размер")
  var mySwiper = new Swiper ('.bm-slider-category', {
    // Optional parameters
    direction: 'horizontal',
    loop: true,
    speed: 800,
    spaceBetween: 20,
    slidesPerView: 4,

    // Navigation arrows
    navigation: {
      nextEl: '.bm-slider-category__button-next',
      prevEl: '.bm-slider-category__button-prev',
    },



  });
} else if (slideWidth > 767 && slideWidth < 1023) {
  var mySwiper = new Swiper ('.bm-slider-category', {
    // Optional parameters
    direction: 'horizontal',
    loop: true,
    speed: 800,
    spaceBetween: 20,
    slidesPerView: 3,

    // Navigation arrows
    navigation: {
      nextEl: '.bm-slider-category__button-next',
      prevEl: '.bm-slider-category__button-prev',
    },

  });
} else if (slideWidth > 319 && slideWidth < 767) {
  var mySwiper = new Swiper ('.bm-slider-category', {
    // Optional parameters
    direction: 'horizontal',
    loop: true,
    speed: 800,
    spaceBetween: 20,
    slidesPerView: 1,
    autoplay: {
      delay: 5000,
    },

    // Navigation arrows
    navigation: {
      nextEl: '.bm-slider-category__button-next',
      prevEl: '.bm-slider-category__button-prev',
    },

  });
} else {
  var mySwiper = new Swiper ('.bm-slider-category', {
    // Optional parameters
    direction: 'horizontal',
    loop: true,
    speed: 800,
    spaceBetween: 20,
    slidesPerView: 5,

    // Navigation arrows
    navigation: {
      nextEl: '.bm-slider-category__button-next',
      prevEl: '.bm-slider-category__button-prev',
    },



  });
}