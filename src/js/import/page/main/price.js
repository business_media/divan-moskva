import $ from "jquery";
import Swiper from "swiper";

// $(document).ready(function () {
//     console.log("Ghbdtn мир");
//   //initialize swiper when document ready
//   var mySwiper = new Swiper ('.bm-price__slider', {
//       // Optional parameters
//       direction: 'horizontal',
//       loop: true,
//       speed: 800,
//       spaceBetween: 30,
//       slidesPerView: 3,
  
//       // Navigation arrows
//       navigation: {
//         nextEl: '.bm-price__button-next',
//         prevEl: '.bm-price__button-prev',
//       },


  
//     });
// });

var slideWidth = $(window).width();

if (slideWidth > 319 && slideWidth < 767) {
  var mySwiper = new Swiper ('.bm-price__slider', {
    // Optional parameters
    direction: 'horizontal',
    loop: true,
    speed: 800,
    spaceBetween: 30,
    slidesPerView: 1,

    autoplay: {
      delay: 5000,
    },

    // Navigation arrows
    navigation: {
      nextEl: '.bm-price__button-next',
      prevEl: '.bm-price__button-prev',
    },



  });
} else {
  var mySwiper = new Swiper ('.bm-price__slider', {
    // Optional parameters
    direction: 'horizontal',
    loop: true,
    speed: 800,
    spaceBetween: 30,
    slidesPerView: 3,

    // Navigation arrows
    navigation: {
      nextEl: '.bm-price__button-next',
      prevEl: '.bm-price__button-prev',
    },



  });
}
