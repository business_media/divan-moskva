import $ from "jquery";
import Swiper from "swiper";

var galleryThumbs = new Swiper('.bm-preview-product__bottom-wrapper', {
    spaceBetween: 20,
    slidesPerView: 4,
    freeMode: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
  });
  var galleryTop = new Swiper('.bm-preview-product__wrapper', {
    spaceBetween: 20,
    navigation: {
      nextEl: '.bm-preview-product__button-next',
      prevEl: '.bm-preview-product__button-prev',
    },
    thumbs: {
      swiper: galleryThumbs
    }
  });