import $ from "jquery";
require('jquery-ui.1.11.1/ui/slider');
console.log("Урааааа!");


$( function() {
    $( "#slider-range" ).slider({
      range: true,
      min: 0,
      max: 500,
      values: [ 0, 500 ],
      stop: function(event, ui) {
        jQuery("input#minCost").val($("#slider-range").slider("values",0));
        jQuery("input#maxCost").val($("#slider-range").slider("values",1));
      },
      slide: function(event, ui){
        jQuery("input#minCost").val(jQuery("#slider-range").slider("values",0));
        jQuery("input#maxCost").val(jQuery("#slider-range").slider("values",1));
        }
    });
  } );

  $("input#minCost").change(function(){
    var value1=jQuery("input#minCost").val();
    var value2=jQuery("input#maxCost").val();
  
      if(parseInt(value1) > parseInt(value2)){
      value1 = value2;
      jQuery("input#minCost").val(value1);
    }
    jQuery("#slider-range").slider("values",0,value1);	
  });
  
    
  $("input#maxCost").change(function(){
    var value1=jQuery("input#minCost").val();
    var value2=jQuery("input#maxCost").val();
    
    if (value2 > 500) { value2 = 500; $("input#maxCost").val(500)}
  
    if(parseInt(value1) > parseInt(value2)){
      value2 = value1;
      $("input#maxCost").val(value2);
    }
    $("#slider-range").slider("values",1,value2);
  });