import $ from "jquery";
import Swiper from "swiper";

// $(document).ready(function () {
//     console.log("Ghbdtn мир");
//   //initialize swiper when document ready
//   var mySwiper = new Swiper ('.bm-about__member-slider', {
//       // Optional parameters
//       direction: 'horizontal',
      
//       speed: 800,
//       spaceBetween: 40,
//       slidesPerView: 4,
//       pagination: {
//         el: '.bm-about__pagination',
//         clickable: true
//       },
  
//       // Navigation arrows
//     });
// });

var slideWidth = $(window).width();

if (slideWidth > 1023 && slideWidth < 1439) {

    var mySwiper = new Swiper ('.bm-about__member-slider', {
        // Optional parameters
        direction: 'horizontal',
        
        speed: 800,
        spaceBetween: 40,
        slidesPerView: 3,
        pagination: {
          el: '.bm-about__pagination',
          clickable: true
        },
    
        // Navigation arrows
      });
} else if (slideWidth > 767 && slideWidth < 1023) {
    var mySwiper = new Swiper ('.bm-about__member-slider', {
        // Optional parameters
        direction: 'horizontal',
        
        speed: 800,
        spaceBetween: 40,
        slidesPerView: 2,
        pagination: {
          el: '.bm-about__pagination',
          clickable: true
        },
    
        // Navigation arrows
      });
} else if (slideWidth > 319 && slideWidth < 767) {
    var mySwiper = new Swiper ('.bm-about__member-slider', {
        // Optional parameters
        direction: 'horizontal',
        
        speed: 800,
        spaceBetween: 40,
        slidesPerView: 1,
        pagination: {
          el: '.bm-about__pagination',
          clickable: true
        },
    
        // Navigation arrows
      });
} else {
    var mySwiper = new Swiper ('.bm-about__member-slider', {
        // Optional parameters
        direction: 'horizontal',
        
        speed: 800,
        spaceBetween: 40,
        slidesPerView: 4,
        pagination: {
          el: '.bm-about__pagination',
          clickable: true
        },
    
        // Navigation arrows
      });
}